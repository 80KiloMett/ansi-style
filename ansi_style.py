class AnsiStyle():

    def __init__(self, *styles):
        self.style = ';'.join([str(style) for style in styles])

    def __call__(self, string):
        return f"\33[{self.style}m{string}\33[0m"

    def __add__(self, other):
        return AnsiStyle(self.style, other.style)

class Styles():
    black = AnsiStyle(30)
    red = AnsiStyle(31)
    green = AnsiStyle(32)
    yellow = AnsiStyle(33)
    blue = AnsiStyle(34)
    purple = AnsiStyle(35)
    cyan = AnsiStyle(36)
    white = AnsiStyle(37)

    black2 = AnsiStyle(90)
    red2 = AnsiStyle(91)
    green2 = AnsiStyle(92)
    yellow2 = AnsiStyle(93)
    blue2 = AnsiStyle(94)
    purple2 = AnsiStyle(95)
    cyan2 = AnsiStyle(96)
    white2 = AnsiStyle(97)

    blackbg = AnsiStyle(40)
    redbg = AnsiStyle(41)
    greenbg = AnsiStyle(42)
    yellowbg = AnsiStyle(43)
    bluebg = AnsiStyle(44)
    purplebg = AnsiStyle(45)
    cyanbg = AnsiStyle(46)
    whitebg = AnsiStyle(47)

    black2bg = AnsiStyle(100)
    red2bg = AnsiStyle(101)
    green2bg = AnsiStyle(102)
    yellow2bg = AnsiStyle(103)
    blue2bg = AnsiStyle(104)
    purple2bg = AnsiStyle(105)
    cyan2bg = AnsiStyle(106)
    white2bg = AnsiStyle(107)

    bold = AnsiStyle(1)
    italic = AnsiStyle(3)
    underline = AnsiStyle(4)
    inverse = AnsiStyle(7)

if __name__ == "__main__":
    print(Styles.red("hello"))
    print(Styles.underline("world"))
    red_ul = Styles.red + Styles.underline
    print(red_ul("hello world"))
    red_ul_blue_bg = red_ul + Styles.bluebg
    print(red_ul_blue_bg("hello world, this is a test."))
